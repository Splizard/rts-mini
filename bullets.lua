local Bullets = {}

local Bullet = {}
Bullet.__index = Bullet
newBullet = function(weapon, x, y, x2, y2, time, id, team)
	local bullet = {}
	setmetatable(bullet, Bullet)
	
	bullet.x = x
	bullet.y = y
	bullet.x2 = round(x2)
	bullet.y2 = round(y2)
	bullet.r = math.angle(x,y,x2,y2)
	bullet.weapon = weapon
	bullet.start = time
	bullet.id = id
	bullet.team = team
	
	local v = movePlayer(x,y,x2,y2)
	bullet.vx = v.x
	bullet.vy = v.y
	bullet.speed = weapon.speed*100
	
	--Get collision and collided unit.
	local t, e = collisions.bullet(bullet, time)
	bullet.c = t
	bullet.e = e
	--print("time",bullet.c)
	
	table.insert(Bullets, bullet)
	--print("fire")
	return bullet
end

function Bullet:getPosition(t)
	local time = (t or love.timer.getTime()+serverTimeDelta) - self.start
	return self.x+(self.vx*self.speed*time), self.y+(self.vy*self.speed*time)
end

function Bullet:draw(lg)
	local x, y = self:getPosition()
	lg.draw(images[self.weapon.id][1], x, y, self.r, 1, 1, 5, 5)
end

function Bullets:update()	
	local time = love.timer.getTime()+serverTimeDelta
	for i,bullet in pairs(Bullets) do
		if type(bullet) ~= "function" then
			if bullet.c and time > bullet.c then
				if bullet.e and bullet.e.team ~= bullet.team then
					bullet.e:hit(bullet.weapon)
					if client then
						print(bullet.e.id)
					end
				end
				--print("hit")
				table.remove(Bullets, i)
			end
			local x, y = bullet:getPosition()
			if x > mapWidth or x < 0 or y > mapHeight or y < 0 then
				table.remove(Bullets, i)
			end 
		end
	end
end

function Bullets:draw(lg)
	local debug = debug_collisions 
	for i,bullet in ipairs(Bullets) do
		bullet:draw(lg)
		local x,y = bullet:getPosition(bullet.c)
		if debug and bullet.e then
			lg.line(bullet.x, bullet.y, x,y)
		end
	end
end

return Bullets
