hud = {}

local tBarPos = 0

alphaType = "unit"

function hud.draw(lg)
	--Bar animation
	if bar == true and alphaUnit ~= nil then
		if barPos >= 100 then barPos = 100 else barPos = barPos + 10 end
		if ((alphaType == "building" and buildingsInGame[alpha] and buildingsInGame[alpha].team == player[control].team) or (alphaType == "unit" and unitsInGame[alpha] and units[unitsInGame[alpha].id].builds)) and barPos >= 100 then
			if tBarPos >= windowHeight then tBarPos = windowHeight else tBarPos = tBarPos + 50 end
		else
			if tBarPos > 0 then tBarPos = tBarPos - 50 else tBarPos = 0 end
		end
	else
		if tBarPos > 0 then tBarPos = tBarPos - 50 else tBarPos = 0 end
		if barPos > 0 and tBarPos == 0 then barPos = barPos - 10 elseif tBarPos == 0 then barPos = 0 end
	end

	--Training HUD backround
	if state == "game" then
		lg.setColor( 150, 150, 150)
		lg.rectangle("fill",10, 0, 80, tBarPos) --HUD
	end

	--Training HUD
	if alpha ~= nil and alphaType == "building" and state == "game" and buildingsInGame[alpha].team == player[control].team and buildingsInGame[alpha].built == true then
		x, y = camera:mousePosition()
		local n = 60
		for i = 1, table.getn(buildings[buildingsInGame[alpha].id].training) do
			local v = buildings[buildingsInGame[alpha].id].training[i]
			if CheckCollision(x,y,1,1,50+camera.x-100+barPos-40, 177+n+camera.y-windowHeight+tBarPos,80,60) then
				lg.print(units[v].name.." $"..units[v].cost.."\n"..units[v].description, 2+0+barPos, 177+n+0-windowHeight+tBarPos)
			end

			--Scale unit to fit
			local scaleX = 1
			local scaleY = 1
			if races[units[v].race].box[2] > 80 then scaleX = 80/races[units[v].race].box[2] end
			if races[units[v].race].box[3] > 60 then scaleY = 60/races[units[v].race].box[3] end
			if scaleX < scaleY then scale = scaleX else scale = scaleY end

			if units[v].cost > money then
				lg.setColorMode("modulate")
				lg.setColor( 150, 150, 150)
			end
			if images[units[v].weapon][2] ~= nil then
				lg.draw(images[units[v].weapon][2],50+0-100+barPos, 177+n+0-windowHeight+races[units[v].race].origin[1]+tBarPos, math.pi/2,scale,scale,races[units[v].race].origin[1],races[units[v].race].origin[1]) --Weapon
			end
			lg.draw(images[units[v].race].gun,50+0-100+barPos, 177+n+0-windowHeight+races[units[v].race].origin[1]+tBarPos, math.pi/2,scale,scale,races[units[v].race].origin[1],races[units[v].race].origin[1])
			lg.setColorMode("modulate")
			if buildingsInGame[alpha].team == player[control].team then
				lg.setColor( 0, 0, 255)
			else
				lg.setColor( 255, 0, 0)
			end
			lg.draw(images[units[v].race].body,50+0-100+barPos, 177+n+0-windowHeight+races[units[v].race].origin[1]+tBarPos, math.pi/2,scale,scale,races[units[v].race].origin[1],races[units[v].race].origin[1])
			lg.setColor(255,255,255)
			if units[v].cost > money then
				lg.setColorMode("modulate")
				lg.setColor( 150, 150, 150)
			end
			lg.draw(images[units[v].race].head,50+0-100+barPos, 177+n+0-windowHeight+races[units[v].race].origin[1]+tBarPos, math.pi/2,scale,scale,races[units[v].race].origin[1],races[units[v].race].origin[1])
			lg.setColor(255,255,255)
			n = n+60
		end
	end

	--Builders HUD
	if alpha and alphaType == "unit" and unitsInGame[alpha] and units[unitsInGame[alpha].id].builds and state == "game" and unitsInGame[alpha].team == player[control].team then
		x, y = camera:mousePosition()
		local n = 20
		for i = 1, table.getn(units[unitsInGame[alpha].id].builds) do
			local v = units[unitsInGame[alpha].id].builds[i]
			if CheckCollision(x,y,1,1,50+camera.x-100+barPos-40, 177+n+camera.y-windowHeight+tBarPos+40,80,80) then
				lg.print(buildings[v].name.." $"..buildings[v].cost.."\n"..buildings[v].description, 2+barPos, 177+n-windowHeight+tBarPos+40)
			end
			if buildings[v].cost > money then
				lg.setColorMode("modulate")
				lg.setColor( 150, 150, 150)
			end

			--Scale building to fit
			local scaleX = 1
			local scaleY = 1
			if buildings[v].box[2] > 80 then scaleX = 80/buildings[v].box[2] end
			if buildings[v].box[3] > 80 then scaleY = 80/buildings[v].box[3] end
			if scaleX < scaleY then scale = scaleX else scale = scaleY end

			lg.draw(images[buildings[v].id][1],50+0-100+barPos, 177+n+0-windowHeight+buildings[v].origin[1]+tBarPos, math.pi,scale,scale,buildings[v].origin[1],buildings[v].origin[2])
			lg.setColor(255,255,255)
			n = n+80
		end
	end

	--Airstrike Hud
		--lg.setColor( 0, 0, 0)
		--lg.rectangle("fill",(windowWidth/2)-100, 30, 200, 52) --Bar
		lg.setColor( 50, 50, 50)
		lg.rectangle("fill",(windowWidth/2)-100, 30, 200, 50) --Bar
		
		 --Airstrike Huds Triangles--
		--lg.setColor( 100, 100, 100)
		--lg.triangle("fill", (windowWidth/2)-100-52, 30, (windowWidth/2)-100, 52+30, (windowWidth/2)-100, 30 )
		--lg.triangle("fill", (windowWidth/2)+100+52, 30, (windowWidth/2)+100, 52+30, (windowWidth/2)+100, 30 )
		lg.setColor( 50, 50, 50)
		lg.polygon("fill", (windowWidth/2)-100-50, 30, (windowWidth/2)-100, 50+30, (windowWidth/2)-100, 30 )
		lg.polygon("fill", (windowWidth/2)+100+50, 30, (windowWidth/2)+100, 50+30, (windowWidth/2)+100, 30 )
		
		lg.setColor( 255, 255, 255)
		lg.rectangle("fill",(windowWidth/2)-100, 30, 46, 45) --Bar
		lg.rectangle("fill",(windowWidth/2)-100+50+1, 30, 46, 45) --Bar
		lg.rectangle("fill",(windowWidth/2)+2, 30, 46, 45) --Bar
		lg.rectangle("fill",(windowWidth/2)+50+3, 30, 46, 45) --Bar

	--HUD backround
	lg.setColor( 50, 50, 50)
	lg.rectangle("fill",0, 0, windowWidth, 30) --Panel
	lg.rectangle("fill",0, 0, barPos, 200) --HUD
	lg.polygon("fill", 0-100+barPos, 0+200, 0+barPos, 0+200, 0-100+barPos, 0+250 ) --HUD Triangle
	lg.rectangle("line",0-100+barPos, 0+30, 99, 99) --Image backround border

	--Print FPS (debugging purposes).
	--lg.print("RTS mini 0.1", windowWidth-90, 4+0)
	--lg.print("FPS: "..love.timer.getFPS().." $"..money, windowWidth-91, 16+0)

	lg.setColor( 255, 255, 255)
	lg.rectangle("fill",1+0-100+barPos, 1+0+30, 98, 98) --Image backround

	--Unit HUD
	if alphaUnit and alphaType == "unit" and Units[alpha] and unitsInGame[alpha].selected then
		--drawUnitsHud(lg)

	--Building HUD
	elseif alpha ~= nil and alphaType == "building" and buildingsInGame[alpha] ~= nil and buildingsInGame[alpha].selected then
		--drawBuildingsHud(lg)
	end
end
