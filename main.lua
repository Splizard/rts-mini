--RTS MINI---

mapWidth = 100*10
mapHeight = 100*10
love.physics.setMeter(64)
world = love.physics.newWorld(0, 0, true)
world:setCallbacks(beginContact, endContact)

function love.load()
	require("util")
	require("api")
	Units = require("units")
	require "networking"
	require "camera"
	Objects = require "objects"
	
	if arg[2] == "client" then
		client = true
	end
	
	config = game.loadConfig()
	
	
	--Fullscreen function
	love.graphics.setBackgroundColor(0,0,0)
	modes = love.window.getFullscreenModes()
	table.sort(modes, function(a, b) return a.width*a.height < b.width*b.height end)   -- sort from smallest to largest
	table.getn(modes)
	if config.resolution then
		windowWidth,windowHeight = config.resolution:match("^(%d*)x(%d*)")
	else
		windowWidth = modes[table.getn(modes)].width and 800
		windowHeight = modes[table.getn(modes)].height and 600
	end
	--love.graphics.setMode(1024,768, false, true, 0)
	love.window.setMode(windowWidth,windowHeight, {
		fullscreen = config.fullscreen or false, 
		vsync = true, 
	}) 
	--set the window dimensions to 650 by 650 with no fullscreen, vsync on, and no antialiasing
	windowWidth, windowHeight = love.window.getMode( )
	
	--love.graphics.setBackgroundColor( 75, 145, 35)
	
	--Spawn some units.
	RTS.spawnUnit('commander', 1, 50, 50)
	RTS.spawnUnit('commander', 1, 100, 100)
	RTS.spawnUnit('commander', 1, 200, 200)
	RTS.spawnUnit('commander', 1, 300, 300)
	RTS.spawnUnit('commander', 1, 400, 400)
	RTS.spawnUnit('commander', 2, 500, 500)
	RTS.spawnUnit('commander', 2, 600, 600)
	
	RTS.spawnObject('Tree', 100, 300)
	
	--Menu stuff.
	require "menu"
	font = fonts("mono",14)
	state = "menu"
	menu.load()
	menu.logo = love.graphics.newImage("data/images/menu.png")
	
	--Networking
	connected = false
	
	--Game
	controlling = 1 --Team we are controlling.
	
	--HUD.
	require "hud"
	barPos = 0
end

function love.mousepressed(x, y, button)
	x, y = camera:mousePosition() --Get real coordinates
	if button == "l" then
		--net.event("select", x, y)
		Units:select(x,y)
	elseif button == "r" then
		local unit = Units:getUnitByPos(x,y)
		if unit and unit.team ~= controlling then
			net.event("newTarget", unit.id)
		else
			net.event("newPath", round(x), round(y))
		end
		--Units:newPath(x,y)
	end
end

function love.keypressed(key, unicode)
	if key == "d" then
		debug_collisions = not debug_collisions
	end
	if state ~= "game" then
		menu.keypressed(key, unicode)
	end
end

function love.update(dt)
	if state == "game" then
		if client then
			net.client(dt)
		else
			net.host(dt)
		end
	
		--Scroll map
		if love.keyboard.isDown("up") then--or (love.mouse.getY() <= windowHeight-(windowHeight-1)) then
			if camera.y + windowHeight > 0 then camera:move(0,-20) end
		end
		if love.keyboard.isDown("down") then--or (love.mouse.getY() >= windowHeight-1) then
			if camera.y < mapHeight then camera:move(0,20) end
		end
		if love.keyboard.isDown("left") then--or (love.mouse.getX() <= windowWidth-(windowWidth-1)) then
			 if camera.x + windowWidth > 0 then camera:move(-20,0) end
		end
		if love.keyboard.isDown("right") then--or (love.mouse.getX() >= windowWidth-1) then
			if camera.x < mapWidth then camera:move(20,0) end
		end

		Units:update(dt)
	end
	
	if state ~= "game" then
		menu.update(dt)
	end
end

start = love.timer.getTime()

function getPos(vx,vy)
	local time = love.timer.getTime() - start
	return vx*time, vy*time
end

function love.draw()
	local lg = love.graphics
	bar = false
	if state == "game" then
		camera:set()

		lg.setColor( 75, 145, 35)
		lg.rectangle("fill", 0, 0, mapWidth, mapHeight)
	
	
		--Draw units and objects.
		--(pcall to catch errors)
		status, err = pcall(Units.draw,Units,lg)
		if not status then io.write("[ERROR] "..err.."\n") end
		status, err = pcall(Objects.draw,Objects,lg)
		if not status then io.write("[ERROR] "..err.."\n") end
		collisions.draw(lg)
	
		camera:unset()
	
		hud.draw(lg)
		Units:drawHud(lg)
		
		lg.setFont(fonts("mono",14))
		if client then
			lg.print("FPS: "..love.timer.getFPS(),windowWidth-90, 16+0)
		else
			lg.print("Host FPS: "..love.timer.getFPS(),windowWidth-90, 16+0)
		end
		
	
	else
		menu.draw(lg)
	end
	
end

function love.quit()
	game.saveConfig(config)
end

