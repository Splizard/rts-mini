--Find angle between two points
function math.angle(x1,y1, x2,y2) return math.atan2(y2-y1,x2-x1) end

--Calculate the distance between two points
function math.dist(x1,y1, x2,y2) return ((x2-x1)^2+(y2-y1)^2)^0.5 end

--Calculate length of a vector.
function math.vlength(vx,vy)
	return math.sqrt(vx*vx + vy*vy)
end

-- Collision detection function.
-- Checks if a and b overlap.
-- w and h mean width and height.
function CheckCollision(ax1,ay1,aw,ah, bx1,by1,bw,bh)
  local ax2,ay2,bx2,by2 = ax1 + aw, ay1 + ah, bx1 + bw, by1 + bh
  return ax1 < bx2 and ax2 > bx1 and ay1 < by2 and ay2 > by1
end

function math.rdist(a1,a2)
	local delta = math.abs(math.deg(a1) - math.deg(a2))
	if delta <= 180 then
		return math.rad(math.deg(a1) - math.deg(a2))
	else
		local d = math.rad((math.deg(a2)+360)-math.deg(a1))
		if d >= 9 then
			return d-7
		else
			return d
		end
	end
end

-- Get the median of a table.
function median( t )
  local temp={}

  -- deep copy table so that when we sort it, the original is unchanged
  -- also weed out any non numbers
  for k,v in pairs(t) do
    if type(v) == 'number' then
      table.insert( temp, v )
    end
  end

  table.sort( temp )

  -- If we have an even number of table elements or odd.
  if math.fmod(#temp,2) == 0 then
    -- return mean value of middle two elements
    return ( temp[#temp/2] + temp[(#temp/2)+1] ) / 2
  else
    -- return middle element
    return temp[math.ceil(#temp/2)]
  end
 end
 
 function mean( t )
  local sum = 0
  local count= 0

  for k,v in pairs(t) do
    if type(v) == 'number' then
      sum = sum + v
      count = count + 1
    end
  end

  return (sum / count)
end

--Finds shortest direction to turn
function rotateClosest(a1,a2)
	local dir = math.deg(a1) - math.deg(a2)
	if dir > 0 and math.abs(dir) <= 180 then return("CCW")
	elseif dir > 0 and math.abs(dir) > 180 then return("CW")
	elseif dir < 0 and math.abs(dir) <= 180 then return("CW")
	elseif dir < 0 and math.abs(dir) > 180 then return("CCW") end
end

--Calculate vector to point
function movePlayer(bodyX,bodyY, x, y)
	local function length(v)
		return math.sqrt(v.x*v.x + v.y*v.y)
	end
	local v = {}
	v.x = x-bodyX
	v.y = y-bodyY -- minus gun from here.
	-- normalize the vector.
	local len = length(v)
	v.x = v.x / len
	v.y = v.y / len
	return v
end

function round(num, idp)
  local mult = 10^(idp or 0)
  return math.floor(num * mult + 0.5) / mult
end

function split(str, delim)
    -- Eliminate bad cases...
    if string.find(str, delim) == nil then return { str } end

    local result,pat,lastpos = {},"(.-)" .. delim .. "()",nil
    for part, pos in string.gfind(str, pat) do table.insert(result, part); lastPos = pos; end
    table.insert(result, string.sub(str, lastPos))
    return result
end

		
--Fonts are loaded on demand when fonts(font, size) is called.
fonts = {}
fonts.__call = function(self, font, size)
	if self[font..tostring(size)] then
		return self[font..tostring(size)]
	else
		local filename
		if font == "regular" then
			filename = "Ubuntu-R"
		elseif font == "mono" then
			filename = "UbuntuMono-B"
		else
			filename = font
		end
		self[font..tostring(size)]=love.graphics.setNewFont("data/fonts/"..filename..".ttf",size)
		return self[font..tostring(size)]
	end
end
setmetatable(fonts, fonts)

game = {}
	--Logs errors to file and stout.
function game.log(txt, mode)
	if mode == nil then mode = "" end
	if mode == "error" then mode = "[ERROR]" end
	if mode == "info"  then mode = "[INFO]"  end
	txt = tostring(txt) or "nil"
	file = love.filesystem.newFile("log.txt")
	file:open('a')
	if mode == "none" then
		file:write(txt)
	else	
		file:write("["..os.date("%X").."] "..mode.." "..txt.."\n")
		file:close()
		io.write(mode.." "..txt.."\n")
	end
end

--Loads config and returns config values inside table.
function game.loadConfig()
	local config = {}
	local err
	file = love.filesystem.newFile("game.cfg")
	if love.filesystem.exists("game.cfg") then
		file:open('r')
		for line in file:lines() do
			i, v = line:match("^(%S*) = (%S*)")
			if not i or not v then
				err = "Config file could not be parsed"
				break
			end
			if v == "true" then v = true end
			if v == "false" then v = false end
			config[i] = v
		end
		return config, err
	else
		return {}, "Config file does not exist yet!"
	end
end

--Saves contents of client.config to file.
function game.saveConfig(config)
	file = love.filesystem.newFile("game.cfg")
	file:open('w')
	for i,v in pairs(config) do
		local t = type(v)
		if t == "string" or t == "number" or t == "boolean" then
			file:write(i.." = "..tostring(v).."\n")
		else
			print(type(v))
		end
	end
end

---Fix love bug
function love.physics.getDistance(f1,f2)
	return math.dist(f1:getBody():getX(),f1:getBody():getY(),f2:getBody():getX(),f2:getBody():getY())
end

--Timers
game.timers_to_add = {}
game.timers = {}

game.time = function(dtime)
	for _, timer in ipairs(game.timers_to_add) do
		table.insert(game.timers, timer)
	end
	game.timers_to_add = {}
	for index, timer in ipairs(game.timers) do
		timer.time = timer.time - dtime
		if timer.time <= 0 then
			timer.func(timer.param)
			table.remove(game.timers,index)
		end
	end
end

function game.after(time, func, param)
	table.insert(game.timers_to_add, {time=time, func=func, param=param})
end
