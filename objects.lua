--Objects are static entities ingame.

ObjectsById = {}
local Objects = {}
local oid = 0

local Object = {}
Object.__index = Object
RTS.spawnObject = function(objectType, x, y, r)
	local object = {}
	setmetatable(object, Object)
	
	--Identification.
	object.id = oid+1
	oid = oid + 1
	
	--Basic.
	object.type = objectType
	object.name = objects[objectType].name
	object.x, object.y, object.r = x, y, r
	
	--Collision.
	object.size = objects[objectType].box[2]
	object.box = objects[objectType].box
	object.origin = objects[objectType].origin 
	
	--Other.
	object.invisible = objects[objectType].invisible
	object.free = true
	
	ObjectsById[object.id] = object
	table.insert(Objects, object)

	return object
end

--Check if object is within camera.
local function unitInBounds(x,y,unit)
	if (x+(unit.origin[1]*camera.scaleX)) < camera.x or 
	(x-(unit.origin[1]*camera.scaleX)) > (camera.x+(windowWidth*camera.scaleX)) then
		return false
	end
	if (y+(unit.origin[2]*camera.scaleY)) < camera.y or 
	(y-(unit.origin[2]*camera.scaleY)) > (camera.y+(windowHeight*camera.scaleY)) then
		return false
	end
	return true
end

function Object:draw(lg)
	local x = self.x
	local y = self.y
	
	--if not (object.invisible) or state == "editor" then 
		--if unitInBounds(x,y,object) then
			--Draw body.
			if images[self.type] and images[self.type][1] then
				lg.draw(images[self.type][1],x,y,0,1,1,self.origin[1],self.origin[2])
			end
		--end
	--end
end

function Objects:draw(lg)
	for id, object in pairs(ObjectsById) do
		object:draw(lg)
	end
end

return Objects
