--DOUBLE SLIT TIME BASED COLLISION--

collisions = {}

local lines = {}

--Find line intersection.
--BugFix: round the numbers as floating point intersections are not accurate.
function findIntersect(l1p1x,l1p1y, l1p2x,l1p2y, l2p1x,l2p1y, l2p2x,l2p2y, seg1, seg2)
 	l1p1x,l1p1y, l1p2x,l1p2y, l2p1x,l2p1y, l2p2x,l2p2y = round(l1p1x),round(l1p1y), round(l1p2x),round(l1p2y), round(l2p1x),round(l2p1y), round(l2p2x),round(l2p2y)
    local a1,b1,a2,b2 = l1p2y-l1p1y, l1p1x-l1p2x, l2p2y-l2p1y, l2p1x-l2p2x
    local c1,c2 = a1*l1p1x+b1*l1p1y, a2*l2p1x+b2*l2p1y
    local det,x,y = a1*b2 - a2*b1
    if det==0 then return false, "The lines are parallel." end
    x,y = (b2*c1-b1*c2)/det, (a1*c2-a2*c1)/det
    if seg1 or seg2 then
        local min,max = math.min, math.max
        if seg1 and not (min(l1p1x,l1p2x) <= x and x <= max(l1p1x,l1p2x) and min(l1p1y,l1p2y) <= y and y <= max(l1p1y,l1p2y)) or
           seg2 and not (min(l2p1x,l2p2x) <= x and x <= max(l2p1x,l2p2x) and min(l2p1y,l2p2y) <= y and y <= max(l2p1y,l2p2y)) then
            return false, "The lines don't intersect."
        end
    end
    return x,y
end

local lines = {}
local collision = {}
local resolve = {}

debug_collisions = false

function collisions.draw(lg)
	if debug_collisions then
		lg.setColor( 255, 0, 0)
		for i,line in pairs(lines) do
			lg.line(line.x1,line.y1,line.x2, line.y2)
		end
		lg.setColor( 0, 255, 0)
		for i,line in pairs(resolve) do
			if line.static then
				lg.setColor( 0, 255, 0)
			else
				lg.setColor( 0, 255, 255)
			end
			--lg.line(line.x1,line.y1,line.x2, line.y2)
		end
		lg.setColor( 0, 0, 255)
		for i,v in pairs(collision) do
			if v.c then
				lg.setColor( 255, 255, 255)
			else
				lg.setColor( 0, 0, 255)
			end
			lg.rectangle("fill",v.x-5,v.y-5, 5, 5)
		end
	end
end


--Detect future collisions for bullet.
function collisions.bullet(bullet, time)
	local nowTime = time
	local collide = {}
	for id1,line1 in pairs(lines) do
		if (not line1.edge) and line1.unit ~= bullet.id then
			local x, y = findIntersect(line1.x1, line1.y1, line1.x2, line1.y2, bullet.x, bullet.y, bullet.x2, bullet.y2, true)
			if (x and y) then
				local distance
				if not line1.diagonal then
					distance = math.dist(bullet.x,bullet.y,x,y)
					--print(distance)
					table.insert(collide, {d=distance, x=x, y=y, a = false, unit = Units:getUnitById(line1.unit), line=line1} )
				end
			end
			
		--Collision with objects. 
		elseif line1.edge and (not line1.wall) and line1.unit ~= bullet.id then
			local x, y = findIntersect(line1.x1, line1.y1, line1.x2, line1.y2, bullet.x, bullet.y, bullet.x2, bullet.y2, true)
			if (x and y) then
				distance = math.dist(bullet.x,bullet.y,x,y)
				table.insert(collide, {d=distance, x=x, y=y, a = false, unit = nil, line=line1} )
			end
		end
	end
	table.sort(collide, function(lhs, rhs) return lhs.d < rhs.d end )
	for i,coll in pairs(collide) do
		if coll.unit then
			local unit = coll.unit
			local vec = math.vlength(bullet.vx*bullet.speed, bullet.vy*bullet.speed)
			local time = coll.d/vec
			local width = (unit.size*2)/vec
			--print(coll.d)
			
			local t = nowTime+(time-(nowTime-bullet.start))
			
			local x, y = bullet:getPosition(t)
			local x2, y2 = unit:getPosition(t)
			--local x, y, x2, y2 = round(x,1), round(y,1), round(x2,1), round(y2,1)
			--print(math.dist(x, y, x2,y2), unit.size)
			if math.dist(x, y, x2,y2) < unit.size*2 then
				--print(nowTime, nowTime+(time-(nowTime-bullet.start)))
				return t, unit
			end
		else
			local vec = math.vlength(bullet.vx*bullet.speed, bullet.vy*bullet.speed)
			local time = coll.d/vec
			
			local t = nowTime+(time-(nowTime-bullet.start))
			return t
		end
	end
end

--Manage global collisions.
--Draws a line for each path for units and calculates future collisions.
function collisions.update(units, bullets, eta)

	local ccc_loop = false

	--Create lines for unit's paths.
	lines = {}
	
	table.insert(lines, {x1=0,y1=0,x2=mapWidth,y2=0, edge=true, wall=true} )
	table.insert(lines, {x1=0,y1=0,x2=0,y2=mapHeight,edge=true, wall=true} )
	table.insert(lines, {x1=mapWidth,y1=0,x2=mapWidth,y2=mapHeight,edge=true, wall=true} )
	table.insert(lines, {x1=0,y1=mapHeight,x2=mapWidth,y2=mapHeight,edge=true, wall=true} )
	
	for i,unit in pairs(units) do
		--print(unit.x,unit.y)
		unit.collisions = {}
		unit.coll_done = false
		
		if unit.path and unit.path.p[1].x then
			local r = math.angle(unit.x,unit.y,unit.path.p[1].x,unit.path.p[1].y)
			--lines[i] = {x1=unit.x, y1=unit.y, x2=unit.path.p[1].x, y2=unit.path.p[1].y, unit=unit.id}
			local weaponX =	0
			local weaponY = unit.size
			local vx = weaponX*math.cos(-r) + weaponY*math.sin(-r)
			local vy = weaponY*math.cos(-r) - weaponX*math.sin(-r)
			--lg.line(self.x,self.y,self.path.p[1].x,self.path.p[1].y)
			table.insert(lines, {x1=unit.x+vx,y1=unit.y+vy,x2=unit.path.p[1].x+vx,y2=unit.path.p[1].y+vy, unit=unit.id} )
			local weaponX =	0
			local weaponY = -unit.size
			local vx2 = weaponX*math.cos(-r) + weaponY*math.sin(-r)
			local vy2 = weaponY*math.cos(-r) - weaponX*math.sin(-r)
			table.insert(lines, {x1=unit.x+vx2,y1=unit.y+vy2,x2=unit.path.p[1].x+vx2,y2=unit.path.p[1].y+vy2, unit=unit.id} )
			table.insert(lines, {x1=unit.x+vx,y1=unit.y+vy,x2=unit.path.p[1].x+vx2,y2=unit.path.p[1].y+vy2, unit=unit.id, bullet = true} )
			table.insert(lines, {x1=unit.x+vx2,y1=unit.y+vy2,x2=unit.path.p[1].x+vx,y2=unit.path.p[1].y+vy, unit=unit.id, bullet = true} )
			
			
			table.insert(lines, {x1=unit.x+vx,y1=unit.y+vy,x2=unit.x+vx2,y2=unit.y+vy2, unit=unit.id ,diagonal = true} )
			table.insert(lines, {x1=unit.path.p[1].x+vx,y1=unit.path.p[1].y+vy,x2=unit.path.p[1].x+vx2,y2=unit.path.p[1].y+vy2, unit=unit.id, diagonal = true} )
			--[[local weaponX =	0
			local weaponY = unit.size/2
			local vx = weaponX*math.cos(-r) + weaponY*math.sin(-r)
			local vy = weaponY*math.cos(-r) - weaponX*math.sin(-r)
			--lg.line(self.x,self.y,self.path.p[1].x,self.path.p[1].y)
			table.insert(lines, {x1=unit.x+vx,y1=unit.y+vy,x2=unit.path.p[1].x+vx,y2=unit.path.p[1].y+vy, unit=unit.id} )
			
			local weaponX =	0
			local weaponY = -unit.size/2
			local vx2 = weaponX*math.cos(-r) + weaponY*math.sin(-r)
			local vy2 = weaponY*math.cos(-r) - weaponX*math.sin(-r)
			table.insert(lines, {x1=unit.x+vx2,y1=unit.y+vy2,x2=unit.path.p[1].x+vx2,y2=unit.path.p[1].y+vy2, unit=unit.id} )]]
			
			
			
			table.insert(lines, {x1=unit.x,y1=unit.y,x2=unit.path.p[1].x,y2=unit.path.p[1].y, unit=unit.id} )
			local width = unit.size
			local x, y = unit.path.p[1].x, unit.path.p[1].y
			--lg.line(self.x-width,self.y-width,self.x+width,self.y-width)
			table.insert(lines, {x1=x-width, y1=y-width, x2=x+width, y2=y-width, unit=unit.id, finish = true} )
			--lg.line(self.x+width,self.y+width,self.x-width,self.y+width)
			table.insert(lines, {x1=x+width, y1=y+width, x2=x-width, y2=y+width, unit=unit.id, finish = true} )
			--lg.line(self.x-width,self.y+width,self.x-width,self.y-width)
			table.insert(lines, {x1=x-width, y1=y+width, x2=x-width, y2=y-width, unit=unit.id, finish = true} )
			--lg.line(self.x+width,self.y-width,self.x+width,self.y+width)
			table.insert(lines, {x1=x+width, y1=y-width, x2=x+width, y2=y+width, unit=unit.id, finish = true} )
		else
			local width = unit.size
			--lg.line(self.x-width,self.y-width,self.x+width,self.y-width)
			table.insert(lines, {x1=unit.x-width, y1=unit.y-width, x2=unit.x+width, y2=unit.y-width, unit=unit.id, static=true} )
			--lg.line(self.x+width,self.y+width,self.x-width,self.y+width)
			table.insert(lines, {x1=unit.x+width, y1=unit.y+width, x2=unit.x-width, y2=unit.y+width, unit=unit.id, static=true} )
			--lg.line(self.x-width,self.y+width,self.x-width,self.y-width)
			table.insert(lines, {x1=unit.x-width, y1=unit.y+width, x2=unit.x-width, y2=unit.y-width, unit=unit.id, static=true} )
			--lg.line(self.x+width,self.y-width,self.x+width,self.y+width)
			table.insert(lines, {x1=unit.x+width, y1=unit.y-width, x2=unit.x+width, y2=unit.y+width, unit=unit.id, static=true} )
		end
	end
	--Create collision box for objects.
	for i,object in ipairs(ObjectsById) do
		local width = object.size
		table.insert(lines, {x1=object.x-width, y1=object.y-width, x2=object.x+width, y2=object.y-width, edge=true, static=true} )
		table.insert(lines, {x1=object.x+width, y1=object.y+width, x2=object.x-width, y2=object.y+width, edge=true, static=true} )
		table.insert(lines, {x1=object.x-width, y1=object.y+width, x2=object.x-width, y2=object.y-width, edge=true, static=true} )
		table.insert(lines, {x1=object.x+width, y1=object.y-width, x2=object.x+width, y2=object.y+width, edge=true, static=true} )
	end
	collision = {}
	for id1,line1 in pairs(lines) do
		for id2,line2 in pairs(lines) do
			if line1.unit ~= line2.unit and (not line1.bullet) and (not line2.bullet) then --and (not (line1.static and line2.static)) then
				local x, y = findIntersect(line1.x1, line1.y1, line1.x2, line1.y2, line2.x1, line2.y1, line2.x2, line2.y2, true, true)
				if (x and y and line1.unit) then
					
					local unit = Units:getUnitById(line1.unit)
					local unit2 = Units:getUnitById(line2.unit)
					local collisions = unit.collisions
					--print("intersect")
					local distance
					if (line1.diagonal and (not line2.diagonal) and (not line2.static) and (not line2.edge)) then
						distance = math.dist(unit:getX(), unit:getY(), unit2:getX(), unit2:getY())
						--print("lol")
					elseif line2.diagonal and (not line1.diagonal) and (not line1.static) and (not line2.edge) then
						distance = math.dist(unit:getX(), unit:getY(), unit2:getX(), unit2:getY())
						--print("rofl")
					--elseif line1.diagonal then
						--local length = math.dist(unit.x, unit.y, unit.path.p[1].x,unit.path.p[1].y)
						--local diagonal_length = math.dist(line1.x1,line1.y1, line1.x2, line1.y2)
						--distance = math.dist(line1.x1,line1.y1,x,y)*(length/diagonal_length)
					else
						distance = math.dist(line1.x1,line1.y1,x,y)
					end
					--local vec = math.vlength( unit.vx, unit.vy )
					--local time = unit.start+(distance/vec)
					
					
					
					--[[local unit2 = Units:getUnitById(line2.unit)
					local distance2 = math.dist(line2.x1,line2.y1,x,y)
					local vec2 = math.vlength( unpack(make_vector(unit,line1.x2,line1.y2)) )
					local time2 = distance2/vec2
					
					local size = unit.size
					print(size, time)
					local size2 = unit2.size
					
					local x1,y1 = unit:getPosition(time, unpack(make_vector(unit,line1.x2,line1.y2)))
					local x2,y2 = unit2:getPosition(time2, unpack(make_vector(unit2,line1.x2,line1.y2)))
					print(x1,x2,y1,y2)]]
					
					--if math.dist(x1, y1, x2, y2) < (size/2)+(size2/2) then
						--print(collisions[1] and (collisions[1].d > distance), collisions[1] and collisions[1].d , distance)
						--if (collisions[1] and collisions[1].d > distance) or (not collisions[1]) then
							table.insert(collisions, {d=distance, x=x, y=y, a = false, unit = Units:getUnitById(line2.unit), line=line1, line2=line2} )
							table.insert(collision, {d=distance, x=x, y=y, a = false, unit = Units:getUnitById(line2.unit), line=line1, c = true})
						--else
							--table.insert(collision, {d=distance, x=x, y=y, a = false, unit = Units:getUnitById(line2.unit), line=line1})
						--end
						
					--else
						--print(math.dist(x1, y1, x2, y2), (size/2)+(size2/2))
					--end
					
					
					--table.sort(collisions)
				end
			end
		end
	end
	
		local nowTime = love.timer.getTime()+serverTimeDelta
	resolve = {}
	
	--Make this a deterministic pairs rundown.
	
	local eyes = {}
	for i,self in pairs(units) do
		table.insert(eyes, i)
	end
	
	table.sort(eyes)
	for eye,id in pairs(eyes) do
		self = units[id]
		
		table.sort(self.collisions, function(lhs, rhs) return lhs.d < rhs.d end)
		
		if not self.coll_done then
		
			collisions.unit(self,nowTime)
		end
	end
	
	if 	ccc_loop then
		--collisions.update( units, bullets, nowTime)
	end
	local cbullets = collisions.bullet
	for i,bullet in pairs(bullets) do
		if type(bullet) ~= "function" then
			bullet.c, bullet.e = cbullets(bullet, eta)
		end
	end
end


--function 

--Manage collisions for specific unit.

--This handles future collisions generated by collisions.update
function collisions.unit(self, nowTime)
	local sync = false
	self.colling = true
	for i, v in pairs(self.collisions) do
		local dist = self.collisions[i].d
		local vec = math.vlength(self.vx, self.vy) 
		--print("s")
		if (self.vx ~= 0 or self.vy ~= 0) and self.path and (not self.collisions[i].line.finish) and (not self.collisions[i].line2.edge)  then
			local unit = self.collisions[i].unit
		--local x, y = unit.x, unit,y
			--local x2, y2 = self:getPosition((nowTime-self.start)+self.start+time)
			--[[if math.dist(unit.x,unit.y,self.path.p[1].x,self.path.p[1].y) and math.dist(unit.x,unit.y,self.x,self.y) < unit.size*3 then
				print("grrr")
				self.path.p[1].x,self.path.p[1].y = self.x, self.y
				self.moving = false
				self.x = self:getX()
				self.y = self:getY()
				self.r = self:getR()
					self.path = nil
				self.vx = 0
				self.vy = 0
				self.vr = 0
				self.rotating = false
				self.start = time
				break
			end]]


			if self.collisions[i].line.diagonal or self.collisions[i].line2.diagonal then
				--[[print("hi")
				local unit = self.collisions[i].unit
				local distance = math.dist(self:getX(nowTime), self:getY(nowTime), unit:getX(nowTime), unit:getY(nowTime))/2
				print(distance)
				--local rotation = math.abs(math.rdist(self.r,math.angle(self.x,self.y,self.path.p[1].x,self.path.p[1].y))/self.vr)
				local movement = distance/math.vlength(self.vx, self.vy)
				local width = (self.size)/math.vlength(self.vx, self.vy)
				local time = movement
				local x, y = self:getPosition(nowTime+(time-(nowTime-self.start)-width*2))
				local x2, y2 = self:getPosition(nowTime)
				if math.dist(self.x, self.y, x, y) >= math.dist(self.x, self.y, x2, y2) then
					self.path.p[1].x,self.path.p[1].y = x, y
					self.path.p[1].x,self.path.p[1].y = round(self.path.p[1].x),round(self.path.p[1].y)
					print("k")
					break
				end]]
				self.collisions[i].d = math.dist(unit:getX(),unit:getY(), self:getX(), self:getY())/2
				local movement = self.collisions[i].d/math.vlength(self.vx, self.vy)
				local time = movement
				local x, y = unit:getPosition(self.start+(nowTime-self.start)+time)
				local x2, y2 = self:getPosition(self.start+(nowTime-self.start)+time)
				local width = (self.size)/math.vlength(self.vx, self.vy)
				if math.dist(x, y, x2, y2) < unit.size then
					local px, py = self:getPosition(self.start+(nowTime-self.start)+time-width)
					self.path = newPath(px, py)
					print("grr")
					sync = true
					if sync and not client then
						net.event("setPath", self.id, self.path.p[1].x, self.path.p[1].y, self.x, self.y)
					end
					ccc_loop = true
					break
				end
			end


			
			local movement = self.collisions[i].d/math.vlength(self.vx, self.vy)
			local rotation = math.abs(math.rdist(self.r,math.angle(self.x,self.y,self.path.p[1].x,self.path.p[1].y))/self.vr)
			local width = (self.size)/math.vlength(self.vx, self.vy)

			local time = movement

			--[[if not unit.colling then
				local px, py = self:getPosition(nowTime+(time-(nowTime-self.start))-width)
				self.path = newPath(px, py)
				sync = true
				if sync and not client then
					net.event("setPath", self.id, self.path.p[1].x, self.path.p[1].y)
				end
				break
			end]]

			local unit = self.collisions[i].unit
			local x, y = unit:getPosition(nowTime+(time-(nowTime-self.start)))
			local x2, y2 = self:getPosition(nowTime+(time-(nowTime-self.start)))
			
			--[[if self.path and math.dist(x,y,x2,y2) < unit.size*2 then
				local x,y = self:getPosition(nowTime+(time-(nowTime-self.start)))
				local i = 1
				while math.dist(x,y,x2,y2) < unit.size*2 do
					self.path.p[1].x,self.path.p[1].y = self:getPosition(nowTime+(time-(nowTime-self.start))-(width*i))
						--self.path.p[1].x,self.path.p[1].y = self:getPosition(self.start+time-width)
					print(math.dist(x,y,x2,y2) < unit.size*2, math.dist(x,y,x2,y2))
					i = i + 1
					x,y = self:getPosition(nowTime+(time-(nowTime-self.start))-(width*i))
					if i > 5 then
						self.path.p[1].x,self.path.p[1].y = self.x, self.y
						break
					end
				end		
			end]]


			if self.collisions[i].line2.finish then
				--x, y = unit.path.p[1].x,unit.path.p[1].y
			end


			local lines2 = {}
			local width = unit.size
			--lg.line(self.x-width,self.y-width,self.x+width,self.y-width)
			table.insert(lines2, {x1=x-width, y1=y-width, x2=x+width, y2=y-width, unit=unit.id, static=true} )
			--lg.line(self.x+width,self.y+width,self.x-width,self.y+width)
			table.insert(lines2, {x1=x+width, y1=y+width, x2=x-width, y2=y+width, unit=unit.id, static=true} )
			--lg.line(self.x-width,self.y+width,self.x-width,self.y-width)
			table.insert(lines2, {x1=x-width, y1=y+width, x2=x-width, y2=y-width, unit=unit.id, static=true} )
			--lg.line(self.x+width,self.y-width,self.x+width,self.y+width)
			table.insert(lines2, {x1=x+width, y1=y-width, x2=x+width, y2=y+width, unit=unit.id, static=true} )

			local lines3 = {}
			local x, y = self:getPosition(nowTime+(time-(nowTime-self.start)))
			--lg.line(self.x-width,self.y-width,self.x+width,self.y-width)
			table.insert(lines3, {x1=x-width, y1=y-width, x2=x+width, y2=y-width, unit=unit.id, static=true} )
			--lg.line(self.x+width,self.y+width,self.x-width,self.y+width)
			table.insert(lines3, {x1=x+width, y1=y+width, x2=x-width, y2=y+width, unit=unit.id, static=true} )
			--lg.line(self.x-width,self.y+width,self.x-width,self.y-width)
			table.insert(lines3, {x1=x-width, y1=y+width, x2=x-width, y2=y-width, unit=unit.id, static=true} )
			--lg.line(self.x+width,self.y-width,self.x+width,self.y+width)
			table.insert(lines3, {x1=x+width, y1=y-width, x2=x+width, y2=y+width, unit=unit.id, static=true} )





			local width = (self.size*2)/math.vlength(self.vx, self.vy)

			local collide = false


			--local x2, y2 = self:getPosition(nowTime)
				--if math.dist(self.x, self.y, x, y) > math.dist(self.x, self.y, x2, y2)

			--local line1 = self.collisions[1].line
			for id,line2 in ipairs(lines2) do
				for id,line3 in ipairs(lines3) do
					local x, y = findIntersect(line2.x1, line2.y1, line2.x2, line2.y2, line3.x1, line3.y1, line3.x2, line3.y2, true, true)
					if (x and y and self.path) then
						local xn, yn = self:getPosition(nowTime)
						--print("s")
						if (not unit.path) and math.dist(self.x, self.y, unit.x, unit.y) < unit.size*4-10 then
							self.path = newPath(self.x, self.y)
							sync = true
							collide = true
							print("lol")
						--elseif line3.finish then
				
						elseif (not unit.path) and self.collisions[i].d < self.size*2 then
							local px, py = self:getPosition(nowTime+(time-(nowTime-self.start)))
							self.path = newPath(px, py)
							sync = true
							collide = true
							print("jk")
						elseif math.dist(line2.x1, line2.y1, x, y) < math.dist(self.x, self.y, x2, y2) then
							local px, py = self:getPosition(nowTime+(time-(nowTime-self.start))-width)
							self.path = newPath(px, py)
							sync = true
							--if (not line1.static) and (not line2.static) then
								ccc_loop = true
							--end
							print("box")
							collide = true
						end
					end
				end
			end

			--if collide then
			local width = self.size

			local x, y = unit:getPosition(nowTime+(time-(nowTime-self.start)))
			if self.collisions[i].line2.finish then
				--x, y = unit.path.p[1].x,unit.path.p[1].y
			end
			--lg.line(self.x-width,self.y-width,self.x+width,self.y-width)
			table.insert(resolve, {x1=x-width, y1=y-width, x2=x+width, y2=y-width, unit=unit.id, static=true} )
			--lg.line(self.x+width,self.y+width,self.x-width,self.y+width)
			table.insert(resolve, {x1=x+width, y1=y+width, x2=x-width, y2=y+width, unit=unit.id, static=true} )
			--lg.line(self.x-width,self.y+width,self.x-width,self.y-width)
			table.insert(resolve, {x1=x-width, y1=y+width, x2=x-width, y2=y-width, unit=unit.id, static=true} )
			--lg.line(self.x+width,self.y-width,self.x+width,self.y+width)
			table.insert(resolve, {x1=x+width, y1=y-width, x2=x+width, y2=y+width, unit=unit.id, static=true} )


			local x, y = self:getPosition(nowTime+(time-(nowTime-self.start)))

			--lg.line(self.x-width,self.y-width,self.x+width,self.y-width)
			table.insert(resolve, {x1=x-width, y1=y-width, x2=x+width, y2=y-width, unit=unit.id, static=false} )
			--lg.line(self.x+width,self.y+width,self.x-width,self.y+width)
			table.insert(resolve, {x1=x+width, y1=y+width, x2=x-width, y2=y+width, unit=unit.id, static=false} )
			--lg.line(self.x-width,self.y+width,self.x-width,self.y-width)
			table.insert(resolve, {x1=x-width, y1=y+width, x2=x-width, y2=y-width, unit=unit.id, static=false} )
			--lg.line(self.x+width,self.y-width,self.x+width,self.y+width)
			table.insert(resolve, {x1=x+width, y1=y-width, x2=x+width, y2=y+width, unit=unit.id, static=false} )


				if collide then 
		
	
				 --print("collision!") break 
			end

		elseif self.collisions[i].line2.edge and (not self.collisions[i].line.finish) and (not self.collisions[i].line.diagonal) then
			local x, y = self:getPosition(nowTime)
			if self.collisions[i].d > (self.size*2) then
				local movement = self.collisions[i].d/math.vlength(self.vx, self.vy)
				local time = movement
				local width = (self.size*2)/math.vlength(self.vx, self.vy)
				local x, y = self:getPosition(self.start+(nowTime-self.start)+time-width)
				self.path = newPath(x, y)
				sync = true
			elseif math.dist(self.x, self.y, x, y) < self.size*2 then
				self.path = newPath(self.x, self.y)
				sync = true
			else
				local px, py = self:getPosition(nowTime)
				self.path = newPath(px, py)
				sync = true
			end
		end
		
		if sync and not client then
			net.event("setPath", self.id, self.path.p[1].x, self.path.p[1].y, self.x, self.y)
		end
	end
	self.coll_done = true
	self.colling = false
end

 
