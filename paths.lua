local Paths = {}


--Load image for waypoint.
local image = love.graphics.newImage("data/images/flag.png")


local Path = {}
Path.__index = Path

newPath = function(x, y)
	local path = {}
	setmetatable(path, Path)
	
	path.visible = false
	
	--First Waypoint.
	path.p = {}
	path.p[1] = {}
	path.p[1].x = x
	path.p[1].y = y

	return path
end

newTarget = function(id)
	local path = {}
	setmetatable(path, Path)
	
	path.visible = false
	
	--First Waypoint.
	path.p = {}
	path.p[1] = {}
	path.p[1].target = id

	return path
end


function Path:draw(lg)
	lg.setColor( 0, 255, 0)
	--lg.setColorMode("modulate")
	for id,waypoint in pairs(self.p) do
		if waypoint.target == nil and waypoint.x ~= nil then lg.draw(image,waypoint.x, waypoint.y, 0,1,1,20,48) end
	end
	lg.setColor(255,255,255)
end
