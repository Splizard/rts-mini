local socket = require "socket"

networking = {}

--Server address.
local ip_address = "localhost"
local port = "31000"


--Networking values.
local updaterate = 0.1
local timer = 0
local second_timer = 0
local udp
local peers = {}

local events = {}
local sync_stream = {}

--Time difference from server.
serverTimeDelta = 0

local ip, port

--Syncs functions from sync_stream table.
local function sync_state_with_network()
	for i, event in ipairs(sync_stream) do
		if event.time <= love.timer.getTime()+serverTimeDelta then
			--print(event.args)
			event.func(unpack(event.args))
			table.remove(sync_stream, i)
		end
	end
end


net = {}

function net.connect(ip, pt)
	ip_address, port = ip, pt
end


function net.time(t)
	return (t or love.timer.getTime())+serverTimeDelta
end

function net.host(dt)
	if not tcp then
		tcp = socket.tcp()
		tcp:bind('*',"31000")
		tcp:settimeout(0)
		tcp:listen()
		ip, port = tcp:getsockname()
	end
	
	local peer
	if not peers[1] then
		--print("sv")
		peer = tcp:accept()
		if peer then
			peer:settimeout(0)
			tcp:setoption('tcp-nodelay', true)
			table.insert(peers, {client=peer ,lag=0})
			--print("as")
		end
	else

		for i,client in pairs(peers) do
			local data
					--print("lol")
		
			--repeat
				--print("s")
				--print(data)
				data = client.client:receive("*l")
		
				if data then
		
					--Break packet into lines.
					for i,data in ipairs(split(data,"\n")) do
						--Strip newline character.
						data = string.gsub(data, "\n", "") 

						--Parse line.
						net.handle(data,i, peers[i].lag, client.client, true)
					end
				end
				--print("d")
			--until not data
		end
	end
	
	--print("w")
	sync_state_with_network()
	
	
	--print("wo")
	timer = timer + dt
	if timer > updaterate then
		for i,client in pairs(peers) do
			client.client:send("ping "..love.timer.getTime().."\n")
			for i,msg in ipairs(events) do
				client.client:send(msg.."\n")
			end
		end
		if peers[1] then
			for i,msg in ipairs(events) do
				net.handle(msg, 1,  peers[1].lag, peers[1].client)
			end
		else
			for i,msg in ipairs(events) do
				net.handle(msg, 1,  0, nil)
			end
		end
		events = {}
		timer = 0
	end
	--print("wf")
	if peer then
		return peer
	end
	--net.manage(dt)
end

function net.client(dt)
	if not tcp then
		tcp = socket.tcp()
		tcp:settimeout(0)
		repeat 
			tcp:connect(ip_address, port)
		--peers["127.0.0.1".."31000"] = {address="127.0.0.1", port="31000", lag=0}
			e, r = tcp:send("ping "..love.timer.getTime().."\n")
		until not r
		table.insert(peers, {client=tcp ,lag=0})
	end
	
	net.manage(dt)
end

function net.event(event, ...)
	parms = {...}
	local lag = 0
	if event == "sel" then
		table.insert(events , "sel "..love.timer.getTime()+serverTimeDelta.." "..parms[1].." "..(tostring(parms[2]) or ""))
	elseif event == "dsel" then
		table.insert(events , "dsel "..love.timer.getTime()+serverTimeDelta)
	elseif event == "newPath" then
		table.insert(events , "newPath "..love.timer.getTime()+serverTimeDelta.." "..parms[1].." "..parms[2])
	elseif event == "setPath" then
		table.insert(events , "setPath "..love.timer.getTime()+serverTimeDelta.." "..parms[1].." "..parms[2].." "..parms[3].." "..parms[4].." "..parms[5])
	elseif event == "newTarget" then
		table.insert(events , "newTarget "..love.timer.getTime()+serverTimeDelta.." "..parms[1])
	end
end

function net.sync(time, func, ...)
	table.insert(sync_stream, {time=time, func=func, args={...}})
end

function inspect(list)
	for i,v in pairs(list) do
		print(i,v)
	end
end

local serverTimes = {}

function net.handle(data,id,lag,clients, incomming)
	cmd, parms = networking.parse(data)
	
	--Set which team the data comes from.
	local team
	if incomming then
		team = (controlling == 1 and 2) or 1
	else
		team = controlling
	end
	
	if cmd then
		if cmd == "ping" then
			clients:send("ackp "..parms[1].." "..love.timer.getTime().."\n")
		elseif cmd == "ackp" then
			peers[id].lag = love.timer.getTime()-tonumber(parms[1])
			if client then	
				--TODO determine an average.
				if not serverTimeDeltaDone then--or math.random(1,10) == 1 then
					serverTimeDelta = tonumber(parms[2]) - tonumber(parms[1])
					table.insert(serverTimes, serverTimeDelta)
					if #serverTimes > 50 then
						serverTimeDelta = mean(serverTimes)
						print("synced time")
						serverTimeDeltaDone = true
					end
				end
			end
				--print("ping: "..love.timer.getTime()-tonumber(parms[1]).."ms Client Time: "..love.timer.getTime().."s Server Time: "..parms[2].."s")
		elseif cmd == "select" then
			net.sync(parms[1]+0.15, Units.select, Units, tonumber(parms[2]), tonumber(parms[3]))
			print("sel")
			--client:send("selectack "..(0.15).." "..parms[1].." "..parms[2].." "..parms[3].."\n")
			--Units:select(parms[1],parms[2])
		elseif cmd == "sel" then
			net.sync(parms[1]+0.15, Units.selectByID, Units, tonumber(parms[2]))
			if tostring(parms[3]) == "true" then
				--print("alpha")
				net.sync(parms[1]+0.15, Units.alpha, Units, tonumber(parms[2]))
			end
			--print("sel")
		elseif cmd == "dsel" then
			net.sync(parms[1]+0.15, Units.deselectAll, Units)
			--client:send("selectack "..(0.15).." "..parms[1].." "..parms[2].." "..parms[3].."\n")
			--Units:select(parms[1],parms[2])
		elseif cmd == "newPath" then
			net.sync(parms[1]+0.15, Units.newPath, Units, tonumber(parms[2]), tonumber(parms[3]), parms[1]-serverTimeDelta+0.15, team)
		elseif cmd == "setPath" then
			Units:Path(tonumber(parms[2]), tonumber(parms[3]), tonumber(parms[4]), tonumber(parms[5]), tonumber(parms[6]))
		elseif cmd == "newTarget" then
			net.sync(parms[1]+0.15, Units.newTarget, Units, tonumber(parms[2]), parms[1]-serverTimeDelta+0.15, team)
		elseif cmd == "setPos" then
			--inspect(parms)
			Units:getUnitById(tonumber(parms[2])):setPosition(tonumber(parms[3]), tonumber(parms[4]) )
			--client:send("selectack "..(0.15).." "..parms[1].." "..parms[2].." "..parms[3].."\n")
			--Units:select(parms[1],parms[2])
		elseif cmd == "selectack" then
			local latency = (love.timer.getTime()-tonumber(parms[2]))/2
			net.sync(love.timer.getTime()-latency+tonumber(parms[1]), Units.select, Units, tonumber(parms[3]), tonumber(parms[4]))
			--udp:sendto("selectack "..tonumber(parms[1])+(0.15).." "..parms[2].." "..parms[3], msg_or_ip, port_or_nil)
			--Units:select(parms[1],parms[2])
		else
		end
	end
end

function net.manage(dt)
	local data
	--repeat
		data = tcp:receive()
		if data then
		
			--Break packet into lines.
			for i,data in ipairs(split(data,"\n")) do
				data = string.gsub(data, "\n", "") --Strip newline character.
				--print(data)
				--Parse line.
				net.handle(data,1, peers[1].lag,tcp, true)
			end
		end
	--until not data
	
	sync_state_with_network()
	
	timer = timer + dt
	if timer > updaterate then
		tcp:send("ping "..love.timer.getTime().."\n")
		for i,msg in ipairs(events) do
			tcp:send(msg.."\n")
		end
		for i,msg in ipairs(events) do
			net.handle(msg, 1,  peers[1].lag, tcp)
		end
		events = {}
		timer = 0
	end
end

--Catch command and parameters from string.
--eg. networking.parse("cmd flag1 flag2") returns: "cmd", {"flag1", "flag2"}
function networking.parse(data)
	local parms = {}
	local cmd, flags = data:match("^(%S*) (.*)")
	if flags then
		repeat
			v, p = flags:match("^(%S*) (.*)")
			if p then
				flags = p
			end
			if v then
				table.insert(parms,v)
			else
				v = flags:match("^(%S*)")
				table.insert(parms,v)
				break
			end
		until false
	else
		cmd = data
	end
	return cmd, parms	
end

--Connects to a udp server and returns connection.
--eg. networking.connect("192.168.1.71", 30001) returns: udp, error
function networking.connect(address, port) 
	udp = socket.udp()
	udp:settimeout(0)
	_, err = udp:setpeername(address, port)
	
	return udp, err
end


--Catches current keystate and returns server parsable cmd.
--eg keyState("a") returns: "a\n" or ""! a\n"
local function keyState(key)
	if love.keyboard.isDown(key) then
		--TODO: use an on keypress event for space bar.
		if key == " " then key = "_" end
		
		return (key.."\n")
	else
		--TODO: use an on keypress event for space bar.
		if key == " " then key = "_" end
		
		return ("! "..key.."\n")
	end
	return ""
end

--This function directly handles all sending and recieving of network commands in TDS mini.
local recieve_time_tracker = 0
function networking.handleClient(dt)
	local text = ""
	--Sends information to server.	
	if ticks > update then
	
		--Send key state changes
		text = text..keyState("w")
		text = text..keyState("a")
		text = text..keyState("s")
		text = text..keyState("d")
		text = text..keyState(" ")
	end
	
	--Localise some variables for speed.
	local split = split
	local tonumber = tonumber

	--Networking receive and parse loop.
	repeat
		data = udp:receive()
		
		--Some helper variables
		local gettingmap = false
		
		--Update received packets or break loop if there is no more data.
		if data then packet = packet + 1 else break end
		
		--print(data)

		--Catch update number and if it is lower then the current update, ignore it.
		local num = data:match("^(%d*)")
		if num ~= "0" then 
			assert(tonumber(num), "The server you tried to connect to is corrupted.")
		end
		if num == "0" or recieve_time_tracker <= tonumber(num) then
			recieve_time_tracker = tonumber(num)
			
			--Break packet into lines.
			for i,data in ipairs(split(data,"\n")) do
				data = string.gsub(data, "\n", "") --Strip newline character.
				--print(data)
				--Parse line.
				cmd, parms = networking.parse(data)
				if cmd then
					if cmd == "mk" then --mk name team x y health rotation
						assert(tonumber(parms[2]) and tonumber(parms[3]) and tonumber(parms[4]) and tonumber(parms[5]), "The server you tried to connect to is corrupted.")
						
						--If the player does not exist create him/her.
						--[[if not player[tonumber(parms[2])] then 
							player[tonumber(parms[2])] = {
								team=tonumber(parms[2]),
								colour = {0, 255, 0},
								keys={},
								cache = {}
							} 
						end]]--

						--Spawn new unit.
						RTS.spawn_unit(parms[1],tonumber(parms[2]),tonumber(parms[3]),tonumber(parms[4]),tonumber(parms[5]))

					elseif cmd == "id"  then
						assert(tonumber(parms[1]), "The server you tried to connect to is corrupted.")
						--Update player control.
						control = tonumber(parms[1])
						
					elseif cmd:match("u%d*")  then --u i name x y rotation team
					
							local dead = parms[2]
							if dead ~= "d" then
								local i = tonumber(parms[1])
								assert(tonumber(parms[1]) and tonumber(parms[3]) and tonumber(parms[4]) and tonumber(parms[5]),
							"The server you tried to connect to is corrupted.")
							
								--Spawn the unit if it does not exist.
								if not unitsInGame[i] then
									RTS.spawn_unit(parms[2],tonumber(parms[6]),tonumber(parms[3]),tonumber(parms[4]),100,tonumber(parms[5]), {}, i)
								end
							
								--Update 
								--unitsInGame[i].buffer[4] = {}
								unitsInGame[i].buffer[3] = unitsInGame[i].buffer[2]
								unitsInGame[i].buffer[2] = unitsInGame[i].buffer[1]
								unitsInGame[i].buffer[1] = unitsInGame[i].buffer[0]
								unitsInGame[i].buffer[0] = {}
								unitsInGame[i].buffer[0].t = love.timer.getTime( )
								unitsInGame[i].buffer[0].x = tonumber(parms[3])
								unitsInGame[i].buffer[0].y = tonumber(parms[4])
								unitsInGame[i].buffer[0].rotate = tonumber(parms[5]) or 0
								
																
								if unitsInGame[i].dead then
									unitsInGame[i].body:setActive(true)
									unitsInGame[i].dead = false
									unitsInGame[i].buffer[4] = unitsInGame[i].buffer[0]
									unitsInGame[i].buffer[3] = unitsInGame[i].buffer[0]
									unitsInGame[i].buffer[2] = unitsInGame[i].buffer[0]
									unitsInGame[i].buffer[1] = unitsInGame[i].buffer[0]
									unitsInGame[i].body:setPosition(tonumber(parms[3]),tonumber(parms[4]))
								end
							
								--Update unit.
								unitsInGame[i].body:setX( tonumber(parms[3]) )
								unitsInGame[i].body:setY( tonumber(parms[4]) )
								unitsInGame[i].rotate = tonumber(parms[5]) or 0
								unitsInGame[i].health = tonumber(parms[7])
								
								--If the player does not exist create him/her.
--								if not player[tonumber(parms[6])] then 
--									player[tonumber(parms[6])] = {
--										team=tonumber(parms[6]),
--										colour = {0, 255, 0},
--										keys={},
--										cache = {}
--									} 
--								end
							else
								local i = tonumber(parms[1])
								destroyUnit(unitsInGame[i],i)
							end
					
					elseif cmd == "sh" then --sh id x y atx aty count
						--Shoot bullet.
						if shcount == nil then shcount=0 end
						assert(tonumber(parms[2]) and tonumber(parms[3]) and tonumber(parms[4]) and tonumber(parms[5]) and tonumber(parms[6]) ,"The server you tried to connect to is corrupted.")
						local id = tonumber(parms[1])
						local unit = unitsInGame[id]
						
						if tonumber(parms[6]) == shcount+1 then
							unitShootClient(id, tonumber(parms[2]), tonumber(parms[3]),tonumber(parms[4]), tonumber(parms[5]))
							text = text .. "ack sh "..tonumber(parms[6]).."\n"
							shcount = tonumber(parms[6])
						end
					elseif cmd == "!sh" then --sh id x y
						--Shoot bullet.
						assert(tonumber(parms[1]) ,"The server you tried to connect to is corrupted.")
						local id = tonumber(parms[1])
						local unit = unitsInGame[id]
						
						unit.shooting = false
					elseif cmd == "map" then --map
						if gotmap == nil then
							gettingmap = true
						end
					elseif cmd == "o" then --o id x y
						assert(tonumber(parms[2]) and tonumber(parms[3]))
						if gettingmap then
							RTS.spawn_object(parms[1],tonumber(parms[2]),tonumber(parms[3]))
						end
						text = text .. "ack map\n"
					elseif not cmd:match("^(%d*)") then
						print("Client: unknown command: "..cmd)
					end
				end
			end
		end
		if msg and msg ~= 'timeout' then
	        error("Network error: "..tostring(msg))
	    end
	    if gettingmap then
	    	gotmap = true
	    end
	until not data
	
	if ticks > update then
		udp:send(text)
		
		ticks = 0
	end
end

--Starts a TDS mini server on another thread.
function networking.host(port)
	--TODO pass port to server thread.
	local server = love.thread.newThread( "server", "server.lua" )
	return server
end

--Checks if server has stopped and logs error.
function networking.handleServer(server)
		err = server:get('error')
		if err then print("[SERVER] "..err) game.log(err,"error") end
end

