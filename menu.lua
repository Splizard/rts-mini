--The Menu system is a thown togther system that could do with a cleanup
--It is very badly commented.

require("gui")

menu = {}
menu.more = 0
menu.shift = 1
menu.init_state = true

--Main menu.
menu.main = {
	items = {
		{"Play", "game"},
		{"Multiplayer", "multiplayer", true},
		--{"Editor", "editor"},
		{"Options", "options", true},
		{"Exit", "quit"},
	},
}

--Multiplayer Menu.
menu.multiplayer = {
	items = {
		{"Host", "host", true},
		{"Join", "multigame"},
		{"Back", "main", true},
	},
}

--Game host menu.
menu.host = {
	items = {
		{"Start", "game"},
		{"Back", "multiplayer", true},
	},
	
	--Little hack to get a "lobby" implemented.
	--this doesn't hold the client.
	title = "Waiting...",
	on_update = function(dt)
		local connected = net.host(dt)
		if connected then
			menu.host.title = "Players Ready"
		end
	end
}


menu.options = {
	items = {
		--{"Splitscreen", "splitscreen", true},
		{"Resolution", "resolutions", true},
		{"Fullscreen", "", false, 
			function()
				config.fullscreen = not (config.fullscreen or false) 
				love.graphics.toggleFullscreen()
			end
		},
		{"Back", "main", true},
	},
}

menu.splitscreen = {}
menu.splitscreen.items = {}
for i=1,4 do
	table.insert(menu.splitscreen.items, {i,"",false,
		function(name) 
			config.splitscreen = tonumber(name)
		end
	})
end
table.insert(menu.splitscreen.items, {"Back", "options", true})

local IPAddressBox
local IPAddressLable

menu.resolutions = {}
menu.resolutions.items = {}
for i,v in ipairs(modes) do
	table.insert(menu.resolutions.items, {v.width.."x"..v.height,"",false, 
		function(name) 
			config.resolution = name
			local x,y = name:match("^(%d*)x(%d*)")
			love.graphics.setMode(x,y, config.fullscreen or false, true, 0)
			windowWidth, windowHeight = love.graphics.getMode( )
			screens = {
				[0]={0,0,windowWidth,windowHeight},
				[1]={
					[1]={0,0,windowWidth,windowHeight}, 
					[2]={0,0,windowWidth/2-2,windowHeight},
					[3]={0,0,windowWidth,windowHeight/2-2},
					[4]={0,0,windowWidth/2-2,windowHeight/2-2}
				},
				[2]={
					[2]={windowWidth/2+2,0,windowWidth/2-2,windowHeight},
					[3]={0,windowHeight/2-2,windowWidth/2-2,windowHeight/2-2},
					[4]={windowWidth/2-2,0,windowWidth/2-2,windowHeight/2-2},
				},
				[3]={
					[3]={windowWidth/2-2,windowHeight/2-2,windowWidth/2-2,windowHeight/2-2},
					[4]={0,windowHeight/2-2,windowWidth/2-2,windowHeight/2-2}
				},
				[4]={
					[4]={windowWidth/2-2,windowHeight/2-2,windowWidth/2-2,windowHeight/2-2}
				}
			}
		end
	})
end
table.insert(menu.resolutions.items, {"Back", "options", true})

--Run this in love.keypressed().
function menu.keypressed(key,unicode)
	gui.keypressed(key, unicode)
	if key == "escape" then
		if state == "game" or state == "editor" then
			state = "menu"
		elseif state == "menu" then
			if menu.list == menu.main then
				love.event.quit()
			else
				menu.list = menu[menu.list.items[#menu.list.items][2]]
			end
		elseif state == "multigame" then
			state = "menu"
		else
			love.event.quit()
		end
	end
	if state == "menu" then
		if key == "up" or key == "w" then
			menu.keyselection = menu.keyselection - 1
		elseif key == "down" or key == "s" then
			menu.keyselection = menu.keyselection + 1
		elseif key == "return" or key == " " then
			if menu.keyselection < menu.more and menu.keyselection >= menu.shift then
				if menu.list.items[menu.keyselection] then
					if menu.list.items[menu.keyselection][3] then
						menu.list = menu[menu.list.items[menu.keyselection][2]]
						menu.shift = 1
						menu.more = 0
					elseif menu.list.items[menu.keyselection][4] then
						menu.list.items[menu.keyselection][4](menu.list.items[menu.keyselection][1])
						--print("function")
					else 
						state = menu.list.items[menu.keyselection][2]
						menu.init_state = true
						--print("state"..menu.list[menu.selection][2])
					end
					if state == "quit" then love.event.quit() end
				end
				menu.keyselection = 0
			elseif menu.keyselection == menu.more then
				menu.shift = menu.shift + menu.more - 1
				if menu.shift > #menu.list.items then
					menu.shift = 1
				end
				menu.keyselection = 0	
			elseif menu.keyselection == menu.more + 1 then
				if menu.list.items[#menu.list][3] then
					menu.list = menu[menu.list.items[#menu.list.items][2]]
					menu.shift = 1
					menu.more = 0
				elseif menu.list.items[#menu.list.items][4] then
					menu.list.items[#menu.list.items][4](menu.list.items[#menu.list.items][1])
				else 
					state = menu.list.items[#menu.list.items][2]
					menu.init_state = true
				end
				menu.keyselection = 0
			end
		end
	end
end

--Run this in love.load()
function menu.load()
	menu.font = fonts("regular",48)
	
	--Variables of what the mouse and keyboard respectively have selected in the menu.
	menu.selection = 0
	menu.keyselection = 0
	
	--Default Menu.
	menu.list = menu.main
end

--Run this in love.update()
function menu.update(dt)
	if state == "menu" then
		if menu.init_state then
			gui.clear()
			menu.init_state = false
		end
		local width, spacing
		if windowWidth == 1920 then	
			width = 738*2
			spacing = 738
		elseif windowWidth <= 800 then
			width = 0
			spacing = 0
		else
			width = 738
			spacing = 0
		end
		--menu.selection = 0
		
		--Run menu update hook.
		if menu.list.on_update then
			menu.list.on_update(dt)
		end
	
		if not click and love.mouse.isDown("l") then
			leftclick = true
		end
		
		--Keep keypress counter from overshooting menu items.
		if menu.keyselection > menu.more+1 then
			menu.keyselection = menu.more+1
		elseif menu.keyselection < menu.shift-1 then
			menu.keyselection = menu.shift-1
		end
	
		for i,v in ipairs(menu.list.items) do
			if i >= menu.shift and (i <= (menu.shift+menu.more+2) or menu.more == #menu.list.items) then
				if (menu.more ~= #menu.list.items or ( menu.more == #menu.list.items and menu.shift ~= 1))
				and CheckCollision(love.mouse.getX(), love.mouse.getY(),1,1,20+spacing, (windowHeight/8*2)+((menu.more-menu.shift)*60)+5, windowWidth-width-40, 48) then
					menu.selection = menu.more
					if leftclick and not love.mouse.isDown("l") then
						--print("more")
								menu.shift = menu.shift + menu.more - 1
								if menu.shift > #menu.list.items then
									menu.shift = 1
								end
						
						leftclick = false
					end
				elseif CheckCollision(love.mouse.getX(), love.mouse.getY(),1,1,20+spacing, (windowHeight/8*2)+((menu.more-menu.shift+1)*60)+5, windowWidth-width-40, 48) then
					menu.selection = menu.more+1
					if leftclick and not love.mouse.isDown("l") then
					--print("quit")
					if menu.list.items[#menu.list.items][3] then
									menu.list = menu[menu.list.items[#menu.list.items][2]]
									menu.shift = 1
									menu.more = 0
								elseif menu.list.items[#menu.list.items][4] then
									menu.list.items[#menu.list.items][4](menu.list.items[#menu.list.items][1])
								else 
									state = menu.list.items[#menu.list.items][2]
									menu.init_state = true
								end
							leftclick = false
															if state == "quit" then love.event.quit() end
					end
				elseif CheckCollision(love.mouse.getX(), love.mouse.getY(),1,1,20+spacing, (windowHeight/8*2)+((i-menu.shift)*60)+5, windowWidth-width-40, 48) then
					menu.selection = i
					if leftclick and not love.mouse.isDown("l") then
						if menu.selection < menu.more then
							if menu.list.items[menu.selection] then
								if menu.list.items[menu.selection][3] then
																	--print("menu "..menu.list[menu.selection][2])
									menu.list = menu[menu.list.items[menu.selection][2]]
									menu.shift = 1
									menu.more = 0
								elseif menu.list.items[menu.selection][4] then
									menu.list.items[menu.selection][4](menu.list.items[menu.selection][1])
									--print("function")
								else 
									state = menu.list.items[menu.selection][2]
									menu.init_state = true
									--print("state"..menu.list[menu.selection][2])
								end
								if state == "quit" then love.event.quit() end
							end
						end
						leftclick = false
					end
				end
			end
		end	
		if menu.shift > #menu.list.items then
			menu.shift = 1
		end
	elseif state == "multigame" then
		if menu.init_state then
			gui.clear()
			IPAddressBox = gui.newTextBox((windowWidth/2)-200, windowHeight/2, 400,20)
			IPAddressBox:setFont(fonts("mono", 18))
			IPAddressBox:setCallback(function(text) 
				gui.clear()
				if text == "" then
					text = "localhost"
				end
				net.connect(text, "31000")
				connected = true
				state = "game"
				client = true
				controlling = 2
			end)

			IPAddressLable = gui.newLable((windowWidth/2)-200, (windowHeight/2)-50,400,"center",{255,255,255})
			IPAddressLable:setText("Server IP Address")
			IPAddressLable:setFont(fonts("regular",36))
			IPAddressBox:focus()
			menu.init_state = false
		end
	end
end

--Run this in love.draw() pass love.graphics
function menu.draw(lg)
	if menu.init_state then return end
	if state == "menu" then
		local width, spacing, imgwidth, imgheight
		if windowWidth >= 1920 then	
			width = 738*2
			spacing = 738
			imgwidth=738
			imgheight=768
		elseif windowWidth == 800 then
			width = 350
			spacing = 0
			imgwidth=350
			imgheight=600
		elseif windowWidth <= 800 then
			width = 0
			spacing = 0
			imgwidth=738
			imgheight=768
		else
			width = 738
			spacing = 0
			imgwidth=738
			imgheight=768
		end
		
		--Left Image
		lg.setColor( 175, 200, 200)
		lg.rectangle("fill",0, 0, width, windowHeight)
		lg.setColor( 255, 255, 255)
		if width == 738*2 then
			lg.draw(menu.logo, 0,windowHeight-imgheight)
		end
		
		--Right Image
		lg.setColor( 175, 200, 200)
		lg.rectangle("fill",windowWidth-imgwidth, 0, width, windowHeight)
		lg.setColor( 255, 255, 255)
		if width ~= 0 then
			lg.draw(menu.logo, windowWidth-imgwidth,windowHeight-imgheight)
		end
	
		--Menu
		lg.setColor( 50, 50, 50)
		lg.rectangle("fill",spacing, 0, windowWidth-width, windowHeight)
		lg.setColor( 100, 100, 100)
		lg.rectangle("fill",spacing+20, 20, windowWidth-width-40, windowHeight-40)

		--Draw menu
		love.graphics.setFont( menu.font )
	
		lg.setColor(255,255,255)
		lg.print(menu.list.title or "RTS mini", (windowWidth-width)/2-100+spacing, (windowHeight/8*2)-(2*60))
		for i,v in ipairs(menu.list.items) do
			if i >= menu.shift then
				lg.setColor( 75, 75, 75)
				if (i == #menu.list.items and menu.shift == 1 and (windowHeight/8*2)+((i-menu.shift)*60)+5 <= windowHeight-(1*60)+5) or
				 (not (i == #menu.list.items and menu.shift ~= 0) and 
				(windowHeight/8*2)+((i-menu.shift)*60)+5 <= windowHeight-(3*60)+5)
				 then
					lg.rectangle("fill",spacing+20, (windowHeight/8*2)+((i-menu.shift)*60)+5, windowWidth-width-40, 48)
					if menu.selection == i or menu.keyselection == i then
						--lg.setColorMode("modulate")
						lg.setColor( 0, 0, 0)
					else
						lg.setColor(255,255,255)
					end
					lg.print(v[1], (windowWidth-width)/2-100+spacing, (windowHeight/8*2)+((i-menu.shift)*60))
					lg.setColor(255,255,255)
				else
					menu.more = i
					lg.rectangle("fill",spacing+20, (windowHeight/8*2)+((i-menu.shift)*60)+5, windowWidth-width-40, 48)
					
					if menu.selection == i or menu.keyselection == i then
						--lg.setColorMode("modulate")
						lg.setColor( 0, 0, 0)
					else
						lg.setColor(255,255,255)
					end
					lg.print("More", (windowWidth-width)/2-100+spacing, (windowHeight/8*2)+((i-menu.shift)*60))
					lg.setColor(255,255,255)
					lg.setColor( 75, 75, 75)
					id = #menu.list.items
					v = menu.list.items[id]
					lg.rectangle("fill",spacing+20, (windowHeight/8*2)+(((i-menu.shift)+1)*60)+5, windowWidth-width-40, 48)
					if menu.selection == i+1 or menu.keyselection == i+1 then
						--lg.setColorMode("modulate")
						lg.setColor( 0, 0, 0)
					else
						lg.setColor(255,255,255)
					end
					lg.print(v[1], (windowWidth-width)/2-100+spacing, (windowHeight/8*2)+(((i-menu.shift)+1)*60))
					lg.setColor(255,255,255)
					break
				end
			end
			menu.more = #menu.list.items + 1
		end
	
		love.graphics.setFont( font )
	elseif state == "multigame" then
		IPAddressBox:draw()
		IPAddressLable:draw()
	end
end
